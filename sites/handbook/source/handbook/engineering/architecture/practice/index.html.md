---
layout: handbook-page-toc
title: "Practices"
---

## Practices

* [*Scalability*](scalability/)
* [*Security Architecture Principles*](/handbook/engineering/security/architecture/#security-architecture-principles)
