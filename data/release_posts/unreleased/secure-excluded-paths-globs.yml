---
features:
  secondary:
  - name: "Exclude paths from Secure scanning with double-star globs"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/#vulnerability-filters'
    reporter: connorgilbert
    stage: secure
    categories:
    - SAST
    - Secret Detection
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/224440'
    description: |
      We've improved the way you can ignore paths in GitLab SAST and Secret Detection scanners.
      Ignoring paths can help you focus on the right findings by ignoring test files, example code, or other code you don't want to scan.

      You can now use double-star glob patterns like `**/*_test.go` or `test/**/fixture*` to exclude paths in:

      - SAST, by using the [`SAST_EXCLUDED_PATHS` variable](https://docs.gitlab.com/ee/user/application_security/sast/#vulnerability-filters).
      - Secret Detection, by using the [`SECRET_DETECTION_EXCLUDED_PATHS` CI/CD variable](https://docs.gitlab.com/ee/user/application_security/secret_detection/#available-cicd-variables).

      We plan to [also support these patterns in Dependency Scanning](https://gitlab.com/gitlab-org/gitlab/-/issues/368052) after delivering this first iteration.
